from bs4 import BeautifulSoup
from flask import Flask
import requests
import random

app = Flask(__name__)


@app.route("/", methods=['GET'])
def home():
    target_url = 'https://en.wikipedia.org/wiki/Zen_of_Python'
    rs = requests.session()
    res = rs.get(target_url, verify=False)
    soup = BeautifulSoup(res.text, 'html.parser')
    return random.choice(soup.select('.poem')[0].text.strip().split('\n'))


if __name__ == '__main__':
    app.run(debug=True)
