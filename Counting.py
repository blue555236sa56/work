urls = [
    "http://www.google.com/a.txt",
    "http://www.google.com.tw/a.txt",
    "http://www.google.com.tw/download/c.jpg",
    "http://www.google.co.jp/a.txt",
    "http://www.google.com/b.txt",
    "http://www.facebook.com/movie/b.txt",
    "http://yahoo.com/123/000/c.jpg",
    "http://gliacloud.com/haha.png",
]

if __name__ == "__main__":
    count_dict = dict()
    for url in urls:
        file_name = url.split("/")[-1]
        if file_name in count_dict:
            count_dict[file_name] += 1
        else:
            count_dict[file_name] = 1
    top_count = 0
    for key, val in count_dict.items():
        if top_count == 3:
            break
        print('{} {}'.format(key, val))
        top_count += 1
